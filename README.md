# Pico Exercise
This project is an exercise for Pico.
The goal is to process Whatsapp and Messenger message:  
  - Make them fit with the Pico message format  
  - Save it into DB  
  - Save it into Cache  
  - Process AI Analysis  
  - Update Web socket  

## Attention!
Functionnalities are not developed (no real Database, no cache etc...)

## Test Exception catching
To try the flow with an exception, pass `True` to one of the process methods.

# Installation
Please, run:  
`composer install`  

The domain or host need to target public/index.php.  

The active URL is {{ your test domain }}/messages in POST request.

It's mainly for PSR-4/Autoload  

For any question: tbenhamou@gmail.com