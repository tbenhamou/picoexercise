<?php

namespace App;

use App\Message;

/**
 * Class in charge of managing the cache
 */
class Cache {

    private function __construct ()
    {
        //
    }

    /**
     * Function in charge to add message to cache
     *
     * @return bool
     */
    public static function add (Message $message, $time)
    {
        return True;
    }

    /**
     * Function in charge to remove message from the cache
     *
     * @return void
     */
    public static function remove (Message $message)
    {
        return True;
    }

}