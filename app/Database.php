<?php

namespace App;

/**
 * Singleton class in charge of managing the Database connection and run the queries
 */
class Database {

    private $connexion;
	private $host;
	private $user;
	private $password;
	private $database;
    private $port;
    
    protected $instance;

    private function __construct(){
        $this->connect();
    }

    /**
     * getInstance method: check if the Database already was initiated.
     * If yes, return the instance.
     * If not, create a new nstance
     *
     * @return Database
     */
    public static function getInstance(): Database {
        if (!isset(self::$instance)) {
          self::$instance = new Database();
        }
        return self::$instance;
      }

      private function connect ()
      {
        //   PDO connection stuff...
      }

      public static function query($query, $params)
      {
        //   This function execute a Query
          return True;
      }

      public function disconnect ()
      {
          $this->connexion = NULL;
      }

}