<?php

namespace App;

class Message
{

    public $origin;
    public $sender_name;
    public $sender_id;
    public $receiver_name;
    public $receiver_id;
    public $message_id;
    public $body;
    public $type;
    public $timestamp;
    
    public function __construct ($receivedMessage) {
        // Based on this parameter, I can define if the provider/origin/source is Whatsapp or Messenger
        // And then, create my generic message instance
        // For the exercise, I consider only text message
        // (but it's really easy to extend the functionnalities for all types of messages)
        if(array_key_exists('sender', $receivedMessage)) {
            $this->convertFacebookToArray($receivedMessage);
        }
        else {
            $this->convertWhatsappToArray($receivedMessage);
        }
    }

    /**
     * Convert the received message from Whatsapp into a Pico Message
     * 
     * @param array $receivedMessage
     * @return void
     */
    public function convertWhatsappToArray ($receivedMessage) {
        
        // A Whatsapp message example:
        // {
        //     "contacts": [ {
        //       "profile": {
        //           "name": "Kerry Fisher"
        //       },
        //       "wa_id": "16315551234"
        //     } ],
        //     "messages":[{
        //       "from": "16315551234",
        //       "id": "ABGGFlA5FpafAgo6tHcNmNjXmuSf",
        //       "timestamp": "1518694235",
        //       "text": {
        //         "body": "Hello this is an answer"
        //       },
        //       "type": "text"
        //     }]
        //   }

        $this->origin = 'whatsapp';
        $this->sender_name = $receivedMessage['contacts'][0]['profile']['name'];
        $this->sender_id = $receivedMessage['contacts'][0]['wa_id'];
        // $this->receiver_name = $receivedMessage[''][''];
        // $this->receiver_id = $receivedMessage[''][''];
        $this->message_id = $receivedMessage['messages'][0]['id'];
        $this->body = $receivedMessage['messages'][0]['text']['body'];
        $this->type = $receivedMessage['messages'][0]['type'];
        $this->timestamp = $receivedMessage['messages'][0]['timestamp'];
    }

    public function convertFacebookToArray ($receivedMessage) {
        // A Facebook message example:
        // {
        //     "sender":{
        //       "id":"<PSID>"
        //     },
        //     "recipient":{
        //       "id":"<PAGE_ID>"
        //     },
        //     "timestamp":1458692752478,
        //     "message":{
        //       "mid":"mid.1457764197618:41d102a3e1ae206a38",
        //       "text":"hello, world!",
        //       "quick_reply": {
        //         "payload": "<DEVELOPER_DEFINED_PAYLOAD>"
        //       }
        //     }
        //   }

        $this->origin = 'facebook';
        $this->sender_id = $receivedMessage['sender']['id'];
        $this->receiver_id = $receivedMessage['recipient']['id'];
        $this->message_id = $receivedMessage['message']['mid'];
        $this->body = $receivedMessage['message']['text'];
        $this->type = 'text';
        $this->timestamp = $receivedMessage['timestamp'];
    }
}