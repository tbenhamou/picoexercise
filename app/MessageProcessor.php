<?php

namespace App;

use App\Cache;
use Exception;
use App\Message;
use App\Database;

/**
 * Class in charge of the processing of the message
 * All the method are returning the instance of the current MessageProcessor object
 * This enable the queueing of the methods
 */
class MessageProcessor
{

    // The message to be processed
    public $message;
    
    // The error, if there is
    public $error = False;

    // The process summary we will return back
    public $processSummary = [];

    public function __construct(Message $message)
    {
        // Saving the message in our instance
        $this->message = $message;
    }

    /**
     * The process function process the message and catch the error is there is one
     *
     * @return void
     */
    public function process (): MessageProcessor
    {
        // The steps of the process
        // The boolean parameter of each method throw (for True) or not (for False)
        // an error in the specific method (just for test purpose)
        try {
            $this->saveDb(False)
                ->saveCache(False)
                ->picoAi(False)
                ->updateWebSocket(False);

        // Returning the error message
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }

        return $this;
    }

    /**
     * saveDb method save the message intot our DB as a part of the process
     *
     * @param boolean $fakeError
     * @return MessageProcessor
     */
    protected function saveDb ($fakeError): MessageProcessor
    {
        if($fakeError)
        {
            throw new Exception('Fail to save into Database');
        }

        // Run the query on the database...
        Database::query('Message insertion into database', $this->message);

        array_push($this->processSummary, 'Saved in database');

        return $this;
    }

    /**
     * The saveCache method save the message in cache
     *
     * @param boolean $fakeError
     * @return MessageProcessor
     */
    protected function saveCache ($fakeError): MessageProcessor
    {
        if($fakeError)
        {
            throw new Exception('Fail to save into cache');
        }

        // Adding the message to cache for 3600 sec
        Cache::add($this->message, 3600);

        array_push($this->processSummary, 'Saved in cache');

        return $this;
    }

    /**
     * the picoAi method process the AI Analysis on the message entity
     *
     * @param boolean $fakeError
     * @return MessageProcessor
     */
    protected function picoAi ($fakeError): MessageProcessor
    {
        if($fakeError)
        {
            throw new Exception('Fail to process AI Analysis');
        }

        array_push($this->processSummary, 'Pico AI Process');

        return $this;
    }

    /**
     * the updateWebSocket method update the web socket
     *
     * @param boolean $fakeError
     * @return MessageProcessor
     */
    protected function updateWebSocket ($fakeError): MessageProcessor
    {
        if($fakeError)
        {
            throw new Exception('Fail to update Web Socket');
        }

        array_push($this->processSummary, 'Web Socket updated');

        return $this;
    }
}

