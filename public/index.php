<?php

// Autoload files using the Composer autoloader.
require_once(__DIR__ . '/../vendor/autoload.php');

use App\Message;
use App\MessageProcessor;

// My API Mini Router
// Based on the Request URL, I defined my POST route /messages
switch($_SERVER['REQUEST_URI']){

    case '/messages':
        // Check method (need to be POST)
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Accept Json content
            $json = file_get_contents('php://input');
            // Getting the content
            $receivedMessage = json_decode($json, true);

            // Createingthe message object
            $message = new Message($receivedMessage);
            // Processing the message
            $processor = new MessageProcessor($message);
            $response = $processor->process();

            header('Content-Type: application/json');

            // If the process succeed, we return a 201 status code (resource created) with the steps done
            if(!$response->error) {
                http_response_code(201);
                echo json_encode(array('status' => 'success', 'details' => $response->processSummary));
            }

            // Else, a 400 HTTP response to declare something went wrong during the process and we provide the error
            else
            {
                http_response_code(400);
                echo json_encode(array('status' => 'failed', 'details' => $response->error));
            }
        }

        // Returning 404 if we try to reach the route with a GET method
        else {
            header('Content-Type: application/json');
            http_response_code(404);
            echo json_encode(array('status' => 'failed', 'details' => 'This route support only POST requests'));
        }
}
